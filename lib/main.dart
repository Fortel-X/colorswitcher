import 'package:flutter/material.dart';
import 'dart:math' as math;

void main() {
  runApp(ColorSwitcher());
}

class ColorSwitcher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Color Switcher',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SwitcherHomePage(title: 'Color Switcher'),
    );
  }
}

class SwitcherHomePage extends StatefulWidget {
  SwitcherHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SwitcherHomePageState createState() => _SwitcherHomePageState();
}

class _SwitcherHomePageState extends State<SwitcherHomePage> {
  var _backgroundColor = 0xff000000;
  var _invertedColor = 0xffffffff;
  var _textColors = [];
  var _letters = ['H', 'e', 'y ', 't', 'h', 'e', 'r', 'e', '!'];
  bool _isTapped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _buildSwitcher(),
    );
  }

  Widget _buildSwitcher() {
    return GestureDetector(
        child: Container(
          color: Color(_backgroundColor),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                Text.rich(
                    TextSpan(
                  children:
                  _getText(),
                ),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                ))
              ],
            ),
            ),
          ),
        onTap: () {
          setState(() {
            _backgroundColor = MyColor.getRandomColor();
            for (int i = 0; i < _textColors.length; i++) {
              _textColors[i] = MyColor.getRandomColor();
            }
          });
        });
  }

  List<TextSpan> _getText(){
    if(_letters.length > _textColors.length) {
      _expandTextColors();
    }
    List<TextSpan> text = [];
    for (var i = 0; i < _letters.length; i++) {
      text.add(TextSpan(text: _letters[i], style: TextStyle(color: Color(_textColors[i]))));
    };
    return text;
  }

  Function _expandTextColors() {
    var countExtra = _letters.length - _textColors.length;
    for (countExtra; countExtra > 0; countExtra--) {
      _textColors.add(0xffffffff);
    }
  }
}

class MyColor {

//  static int _seed = 255;
  static int _maxChannelValue = 255;
  static int _channelCount = 3; // Red, Green, Blue
  static int _channelBitSize = 8;
  static var randomizer = new math.Random();

  static int getRandomColor() {
    int color = 0xff; // Alpha channel
    for (int i = 0; i < _channelCount; i++) {
      color = color << _channelBitSize;
      color = color + getRandom();
    }
    return color;
  }

  // 0 - 255
  static int getRandom() {
    return randomizer.nextInt(_maxChannelValue);
//    _seed = (35780 * _seed + 689);
//    return _seed % _maxChannelValue;
  }
}
