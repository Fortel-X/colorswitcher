# color_switcher

Color Switcher is a pretty simple app for switching color pairs to find the best one up to you.

Background and text colors are complementary that means they complete each other to neutral grey (or black, or white).

To start switching just tap the screen.

##################################################################

Randomizing of colors is performed without any external libraries.
